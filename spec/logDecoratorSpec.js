const Decorator = imports.logDecorator.LogDecorator;

/* eslint-disable no-magic-numbers */
describe('a logDecorator', () => {
    beforeEach(() => {
        const TestClass = class {
            noParamFunction() {
                return false;
            }

            oneParamFunction(param1) {
                if (typeof param1 == 'string') {
                    return param1 + param1;
                }
                if (typeof param1 == 'number') {
                    return 3 * param1;
                }
                return null;
            }

            twoParamFunction(param1, param2) {
                if (param1 === param2) {
                    return true;
                }
                return false;
            }

            varArgsFunction() {
                if (arguments.length === 0) {
                    return this.noParamFunction.apply(this, []);
                }
                if (arguments.length === 1) {
                    return this.oneParamFunction.apply(this, arguments);
                }
                if (arguments.length === 2) {
                    return this.twoParamFunction.apply(this, arguments);
                }
                return null;
            }
        };
        this.testObject = new TestClass();
        this.decorator = new Decorator();

        this.loggedText = '';

        this.testLogFunction = (logText) => {
            this.loggedText += logText;
        };
        this.decorator.logger = this.testLogFunction;
        this.decorator.addLoggingToNamespace(this.testObject);
    });

    it('logs function calls to functions without parameters', () => {
        expect(this.testObject.noParamFunction()).toBeFalsy();
        expect(this.loggedText).toBe('noParamFunction();');
    });

    it('logs function calls to functions with one parameter', () => {
        expect(this.testObject.oneParamFunction('myParam')).toBe('myParammyParam');
        expect(this.loggedText).toBe('oneParamFunction(myParam);');

        this.loggedText = '';

        expect(this.testObject.oneParamFunction(2)).toBe(6);
        expect(this.loggedText).toBe('oneParamFunction(2);');

        this.loggedText = '';

        expect(this.testObject.oneParamFunction(undefined)).toBeNull();
        expect(this.loggedText).toBe('oneParamFunction(undefined);');
    });

    it('logs function calls to functions with two parameters', () => {
        expect(this.testObject.twoParamFunction('myParam1', 'myParam2')).toBeFalsy();
        expect(this.loggedText).toBe('twoParamFunction(myParam1, myParam2);');

        this.loggedText = '';

        expect(this.testObject.twoParamFunction(false, false)).toBeTruthy();
        expect(this.loggedText).toBe('twoParamFunction(false, false);');
    });

    it('logs variable arguments functions with recursion', () => {
        expect(this.testObject.varArgsFunction()).toBeFalsy();
        expect(this.loggedText).toBe('varArgsFunction();noParamFunction();');

        this.loggedText = '';

        expect(this.testObject.varArgsFunction('foo')).toBe('foofoo');
        expect(this.loggedText).toBe('varArgsFunction(foo);oneParamFunction(foo);');

        this.loggedText = '';

        expect(this.testObject.varArgsFunction(3)).toBe(9);
        expect(this.loggedText).toBe('varArgsFunction(3);oneParamFunction(3);');

        this.loggedText = '';

        expect(this.testObject.varArgsFunction('myParam1', 'myParam2')).toBeFalsy();
        expect(this.loggedText).toBe('varArgsFunction(myParam1, myParam2);twoParamFunction(myParam1, myParam2);');

        this.loggedText = '';

        expect(this.testObject.varArgsFunction(true, true)).toBeTruthy();
        expect(this.loggedText).toBe('varArgsFunction(true, true);twoParamFunction(true, true);');
    });

    it('can change its logger', () => {
        expect(this.testObject.varArgsFunction()).toBeFalsy();
        expect(this.loggedText).toBe('varArgsFunction();noParamFunction();');

        this.loggedText = '';
        let localLoggedText = '';
        this.decorator.logger = (text) => {
            localLoggedText += text;
        };

        expect(this.testObject.varArgsFunction()).toBeFalsy();
        expect(localLoggedText).toBe('varArgsFunction();noParamFunction();');
        expect(this.loggedText).toBe('');
    });
});
/* eslint-enable no-magic-numbers */
